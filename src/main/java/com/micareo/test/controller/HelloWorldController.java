package com.micareo.test.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapProperties.Validation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.micareo.test.dto.CellsDto;
import com.micareo.test.service.MyClassLoader;
import com.micareo.test.service.ReadFileService;
import com.micareo.test.service.ValidationService;

@CrossOrigin(origins = "http://localhost:4200")
@Controller
public class HelloWorldController {
	
	@Autowired
	ReadFileService readFileService;
	
	@Autowired 
	ValidationService validationService;
	
	private static final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);
	@GetMapping(path="/getListOfValidFile")
	@ResponseBody
	public List<String> parse() throws IOException {
		MyClassLoader myClassLoader = new MyClassLoader();
		List<String> listValidFile = new ArrayList<String>();
		for (File f : myClassLoader.getResourceFolderFiles("csv")) {
		    logger.info(f.getName());
		    List<CellsDto> listData = readFileService.loadObjectList(CellsDto.class,f.getName());
		    if(validationService.validList(listData)) {
		    	listValidFile.add(f.getName());
		    } else {
		    	logger.info("Wrong Answer");
		    };
		}
		return listValidFile;
	}
	
	@GetMapping(path="/getExperiment")
	@ResponseBody
	public List<CellsDto> getExperiment(@RequestParam("file")String fileName) throws IOException {
		List<CellsDto> listData = readFileService.loadObjectList(CellsDto.class,fileName);
		if(validationService.validList(listData)) {
			return listData;
		} else {
		    return null;
		}
	}
}
