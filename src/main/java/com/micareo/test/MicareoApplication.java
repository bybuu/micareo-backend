package com.micareo.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicareoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicareoApplication.class, args);
	}

}
