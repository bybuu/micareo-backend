package com.micareo.test.dto;


public class CellsDto {

	int id;
	double Measure1;
	double Measure2;
	String Type;
	
	public CellsDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CellsDto(int id, double measure1, double measure2, String type) {
		super();
		this.id = id;
		this.Measure1 = measure1;
		this.Measure2 = measure2;
		this.Type = type;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getMeasure1() {
		return Measure1;
	}
	public void setMeasure1(double measure1) {
		Measure1 = measure1;
	}
	public double getMeasure2() {
		return Measure2;
	}
	public void setMeasure2(double measure2) {
		Measure2 = measure2;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
}
