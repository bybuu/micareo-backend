package com.micareo.test.dto;

public class HelloWorldDto {

	String helloWorld;
	
	public HelloWorldDto(String helloWorld) {
		super();
		this.helloWorld = helloWorld;
	}

	public String getHelloWorld() {
		return helloWorld;
	}

	public void setHelloWorld(String helloWorld) {
		this.helloWorld = helloWorld;
	}
}
