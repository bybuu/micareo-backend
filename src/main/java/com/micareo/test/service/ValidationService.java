package com.micareo.test.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.micareo.test.controller.HelloWorldController;
import com.micareo.test.dto.CellsDto;

@Service
public class ValidationService {
	private static final Logger logger = LoggerFactory.getLogger(ValidationService.class);
	public boolean validList(List<CellsDto> cellsDto) {
		logger.info("start");
		for(CellsDto cellDto : cellsDto) {
			logger.info("cells");
			if (greaterThan0(cellDto.getId()) && between0And100(cellDto.getMeasure1()) && betweenMinus100And100(cellDto.getMeasure2()) && isTypeValid(cellDto.getType())) {
				continue;
			}
			return false;
		}
		return true;
	}
	
	private boolean greaterThan0(int i) {
		return i >= 0;
	}
	
	private boolean between0And100(double d) {
		return d >= 0 && d <=100;
	}
	
	private boolean betweenMinus100And100(double d) {
		return d >= -100 && d <=100;
	}
	
	private boolean isTypeValid(String s) {
		return s.equals("Cancer Cell") || s.equals("White Blood Cell") || s.equals("Other") ;
	}
}
