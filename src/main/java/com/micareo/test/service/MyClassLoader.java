package com.micareo.test.service;

import java.io.File;
import java.net.URL;

public class MyClassLoader {
	
		
	
	public MyClassLoader() {
		super();
		// TODO Auto-generated constructor stub
	}

	public File[] getResourceFolderFiles (String folder) {
		    ClassLoader loader = Thread.currentThread().getContextClassLoader();
		    URL url = loader.getResource(folder);
		    String path = url.getPath();
		    return new File(path).listFiles();
	  }
}
